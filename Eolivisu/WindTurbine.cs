﻿namespace Eolivisu
{
    public class WindTurbine
    {
        #region Properties
        public WindFarm Farm { get; }

        public ushort ID { get; }

        public float Power { get; set; }

        public float Rotation { get; set; }

        public bool IsActive { get; set; }
        #endregion

        #region Constructors
        public WindTurbine(WindFarm farm, ushort id)
        {
            Farm = farm;
            ID = id;
        }

        public void Update() => Farm.GetInfo(this);

        public void Stop() => Farm.Stop(this);
        #endregion
    }
}
