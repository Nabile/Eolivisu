﻿using System.Net;
using Xamarin.Forms;

namespace Eolivisu
{
    using Pages;

    public class App : Application
    {
        public static readonly IPAddress ServerAddress = new IPAddress(new byte[] { 192, 168, 0, 2 }); // Settings for database and farms ?

        public App()
        {
            MainPage = new NavigationPage(new LoginPage());
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
