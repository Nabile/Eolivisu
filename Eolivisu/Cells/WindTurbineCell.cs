﻿using Xamarin.Forms;

namespace Eolivisu.Cells
{
    public class WindTurbineCell : ViewCell
    {
        #region Fields
        private readonly Label idLabel, powerLabel, rotationLabel, activeLabel;
        private readonly Button stopButton;
        #endregion

        #region Constructors
        public WindTurbineCell()
        {
            View = new StackLayout
            {
                Padding = new Thickness(5.0, 0.0),
                Children =
                {
                    (idLabel = new Label()),
                    (powerLabel = new Label()),
                    (rotationLabel = new Label()),
                    (activeLabel = new Label
                    {
                        HorizontalTextAlignment = TextAlignment.Center,
                        TextColor = Color.Black
                    }),
                    (stopButton = new Button
                    {
                        Text = "Arrêter",
                        Command = new Command(() =>
                        {
                            (BindingContext as WindTurbine).Stop();
                            (Parent as ListView).BeginRefresh();
                        })
                    })
                }
            };
        }
        #endregion

        #region Methods
        protected override void OnAppearing()
        {
            WindTurbine turbine = BindingContext as WindTurbine;

            idLabel.Text = $"ID: {turbine.ID}";
            powerLabel.Text = $"Puissance: {turbine.Power} Watts";
            rotationLabel.Text = $"Rotation: {turbine.Rotation} tr/min";
            activeLabel.Text = turbine.IsActive ? "Active" : "Inactive";
            activeLabel.BackgroundColor = turbine.IsActive ? Color.Green : Color.Red;
            stopButton.IsEnabled = turbine.IsActive;

            base.OnAppearing();
        }
        #endregion
    }
}
