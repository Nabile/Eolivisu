﻿using System;
using Xamarin.Forms;
using WeatherData;
using System.Collections.Generic;

namespace Eolivisu.Cells
{
    public class WeatherInfoCell : ViewCell
    {
        #region Fields
        private readonly Label dateLabel, directionLabel, speedLabel, gustLabel;
        #endregion

        #region Constructors
        public WeatherInfoCell()
        {
            View = new StackLayout
            {
                Padding = new Thickness(5.0, 0.0),
                Children =
                {
                    (dateLabel = new Label()),
                    (directionLabel = new Label()),
                    (speedLabel = new Label()),
                    (gustLabel = new Label())
                }
            };
        }
        #endregion

        #region Methods
        protected override void OnAppearing()
        {
            var pair = (KeyValuePair<DateTime, WeatherInfo>)BindingContext;

            dateLabel.Text = $"Date: {pair.Key}";
            directionLabel.Text = $"Direction du vent: {pair.Value.WindDirection} °";
            speedLabel.Text = $"Vitesse du vent: {pair.Value.WindSpeed} km/h";
            gustLabel.Text = $"Rafale du vent: {pair.Value.WindGust} km/h";

            base.OnAppearing();
        }
        #endregion
    }
}
