﻿using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;

namespace Eolivisu
{
    public class WindFarm
    {
        #region Fields
        private readonly IPEndPoint endPoint;

        private const ushort Port = 9002;
        #endregion

        #region Properties
        public IEnumerable<WindTurbine> Turbines
        {
            get
            {
                TcpClient client = GetConnection();

                using (var stream = new BufferedStream(client.GetStream()))
                using (var reader = new BinaryReader(stream))
                using (var writer = new BinaryWriter(stream))
                {
                    writer.Write("GetIDs");
                    stream.Flush();

                    int count = reader.ReadInt32();

                    for (int i = 0; i < count; i++)
                    {
                        yield return new WindTurbine(this, reader.ReadUInt16());
                    }
                }
            }
        }
        #endregion

        #region Constructors
        public WindFarm(IPAddress address)
        {
            endPoint = new IPEndPoint(address, Port);
        }
        #endregion

        #region Methods
        private TcpClient GetConnection()
        {
            TcpClient client = new TcpClient();
            client.Connect(endPoint);

            return client;
        }

        public void GetInfo(WindTurbine turbine)
        {
            TcpClient client = GetConnection();

            using (var stream = new BufferedStream(client.GetStream()))
            using (var reader = new BinaryReader(stream))
            using (var writer = new BinaryWriter(stream))
            {
                writer.Write("GetInfo");
                writer.Write(turbine.ID);
                stream.Flush();

                turbine.Power = reader.ReadSingle();
                turbine.Rotation = reader.ReadSingle();
                turbine.IsActive = reader.ReadBoolean();
            }
        }

        public void Stop(WindTurbine turbine)
        {
            TcpClient client = GetConnection();

            using (var stream = new BufferedStream(client.GetStream()))
            using (var reader = new BinaryReader(stream))
            using (var writer = new BinaryWriter(stream))
            {
                writer.Write("Stop");
                writer.Write(turbine.ID);
                stream.Flush();
            }
        }
        #endregion
    }
}
