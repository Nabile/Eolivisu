﻿using Xamarin.Forms;

namespace Eolivisu.Pages
{
    public class MainPage : ContentPage
    {
        #region Constructors
        public MainPage()
        {
            Title = "Eolivisu";
            Content = new StackLayout
            {
                Children =
                {
                    new Button
                    {
                        Text = WeatherInfoPage.Name,
                        Command = new Command(async () => await Navigation.PushAsync(new WeatherInfoPage(), true))
                    },
                    new Button
                    {
                        Text = ManageWindFarmPage.Name,
                        Command = new Command(async () => await Navigation.PushAsync(new ManageWindFarmPage(), true))
                    }
                }
            };
        }
        #endregion
    }
}
