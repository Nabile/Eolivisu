﻿using System;
using System.Collections.Generic;
using System.Linq;
using StackExchange.Redis;
using WeatherData;
using Xamarin.Forms;

namespace Eolivisu.Pages
{
    using Cells;

    public class WeatherInfoPage : ContentPage
    {
        #region Fields
        private readonly ConnectionMultiplexer connection = ConnectionMultiplexer.Connect(App.ServerAddress.ToString()); // Exception handling ?

        private const string KeyPrefix = "weather:";
        public const string Name = "Informations météo";
        #endregion

        #region Properties
        private IEnumerable<KeyValuePair<DateTime, WeatherInfo>> Entries
        {
            get
            {
                IServer server = connection.GetServer(connection.GetEndPoints().First());
                IDatabase dataBase = connection.GetDatabase();

                foreach (string key in server.Keys(pattern: KeyPrefix + '*'))
                {
                    DateTime date = DateTimeOffset.FromUnixTimeMilliseconds(long.Parse(key.Replace(KeyPrefix, string.Empty))).DateTime.ToLocalTime();
                    string[] values = dataBase.StringGet(key).ToString().Split('|');
                    int index = 0;

                    yield return new KeyValuePair<DateTime, WeatherInfo>(date, new WeatherInfo
                    {
                        WindDirection = ushort.Parse(values[index++]),
                        WindGust = float.Parse(values[index++]),
                        WindSpeed = float.Parse(values[index++])
                    });
                }
            }
        }
        #endregion

        #region Constructors
        public WeatherInfoPage()
        {
            ListView list = null;

            Title = Name;
            Content = new StackLayout
            {
                Children =
                {
                    new Label
                    {
                        Text = "Historique",
                        FontSize = Device.GetNamedSize(NamedSize.Large, typeof(Label)),
                        FontAttributes = FontAttributes.Bold
                    },
                    (list = new ListView
                    {
                        HasUnevenRows = true,
                        IsPullToRefreshEnabled = true,
                        ItemTemplate = new DataTemplate(typeof(WeatherInfoCell)),
                        RefreshCommand = new Command(() =>
                        {
                            list.ItemsSource = Entries.OrderByDescending(e => e.Key);
                            list.EndRefresh();
                        })
                    })
                }
            };

            list.BeginRefresh();
        }
        #endregion
    }
}
