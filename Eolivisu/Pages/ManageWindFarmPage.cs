﻿using Xamarin.Forms;
using System.Linq;

namespace Eolivisu.Pages
{
    using Cells;

    public class ManageWindFarmPage : ContentPage
    {
        #region Fields
        private readonly WindFarm windFarm = new WindFarm(App.ServerAddress);

        public const string Name = "Gestion du parc éolien";
        #endregion

        #region Constructors
        public ManageWindFarmPage()
        {
            ListView list = null;

            Title = Name;
            Content = new StackLayout
            {
                Children =
                {
                    new Label
                    {
                        Text = "Eoliennes",
                        FontSize = Device.GetNamedSize(NamedSize.Large, typeof(Label)),
                        FontAttributes = FontAttributes.Bold
                    },
                    (list = new ListView
                    {
                        HasUnevenRows = true,
                        IsPullToRefreshEnabled = true,
                        ItemTemplate = new DataTemplate(typeof(WindTurbineCell)),
                        RefreshCommand = new Command(() =>
                        {
                            list.ItemsSource = windFarm.Turbines.ToArray().OrderBy(t => t.ID);

                            foreach (WindTurbine turbine in list.ItemsSource)
                            {
                                turbine.Update();
                            }

                            list.EndRefresh();
                        })
                    })
                }
            };

            list.BeginRefresh();
        }
        #endregion
    }
}
