﻿using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using StackExchange.Redis;
using Xamarin.Forms;

namespace Eolivisu.Pages
{
    public class LoginPage : ContentPage
    {
        #region Fields
        private Entry userNameEntry, passwordEntry;
        private readonly ConnectionMultiplexer connection = ConnectionMultiplexer.Connect(App.ServerAddress.ToString()); // Exception handling ?

        private const string KeyPrefix = "account:";
        #endregion

        #region Constructors
        public LoginPage()
        {
            Title = "Connexion";
            Content = new StackLayout
            {
                Padding = new Thickness(5.0, 0.0),
                Children =
                {
                    (userNameEntry = new Entry
                    {
                        Placeholder = "Nom d'utilisateur"
                    }),
                    (passwordEntry = new Entry
                    {
                        IsPassword = true,
                        Placeholder = "Mot de passe"
                    }),
                    new Button
                    {
                        HorizontalOptions = LayoutOptions.Center,
                        Text = "Se connecter",
                        Command = new Command(() =>
                        {
                            string passwordHash;

                            using (SHA1 sha1 = SHA1.Create())
                            {
                                passwordHash = BitConverter.ToString(sha1.ComputeHash(Encoding.Unicode.GetBytes(passwordEntry.Text ?? string.Empty))).Replace("-", string.Empty);
                            }

                            IServer server = connection.GetServer(connection.GetEndPoints().First());
                            IDatabase dataBase = connection.GetDatabase();

                            if (dataBase.StringGet(KeyPrefix + userNameEntry.Text).ToString()?.ToUpper() == passwordHash)
                                Navigation.PushAsync(new MainPage());
                            else
                                DisplayAlert("Erreur", "Identifiants incorrects.", "Retour");
                        })
                    }
                }
            };
        }
        #endregion
    }
}
